import CartParser from './CartParser';

const URL = "D:/\BSA/\lesson12/\jesttesting/";
const pathToFile = `${URL}samples/\cart.csv`;
let parser, validate, parse, calcTotal, parseLine, createError;

beforeEach(() => {
	parser = new CartParser();
	validate = parser.validate.bind(parser);
	parse = parser.parse.bind(parser);
	parseLine = parser.parseLine.bind(parser);
	calcTotal = parser.calcTotal.bind(parser);
	createError = parser.createError.bind(parser);
});

describe('CartParser - unit tests', () => {
	// Add your unit tests here.

	describe('validate', () => {

		test('should return empty array if validation is successful', () => {
			const content = `Product name,Price,Quantity
				Mollis consequat,9.00,2
				Tvoluptatem,10.32,1`;
			expect(validate(content).length).toEqual(0)
		});

		test('should return error (not empty array) if header has unexpected name', () => {
			const content = `HEADER,Price,Quantity
				Mollis consequat,9.00,2
				Tvoluptatem,10.32,1`;
			// expect(validate(content).length).toBeGreaterThan(0);
			expect(validate(content)[0].type).toBe('header');
		});

		test('should return error (not empty array) if row does not have any cell', () => {
			const content = `Product name,Price,Quantity
				9.00,2
				Tvoluptatem,10.32,1`;
			// expect(validate(content).length).toBeGreaterThan(0);
			expect(validate(content)[0].type).toBe('row');
		});

		test('should return error (not empty array) if cell with type=string has empty string', () => {
			const content = `Product name,Price,Quantity
				Mollis consequat,9.00,2
				 ,10.32,0`;

			expect(validate(content)[0].type).toBe('cell');
		});

		test('should return error (not empty array) if cell has negative number or NaN', () => {
			const content = `Product name,Price,Quantity
				Mollis consequat,9.00,2
				Tvoluptatem,-10.32,1`;
			// expect(validate(content).length).toBeGreaterThan(0);
			expect(validate(content)[0].type).toBe('cell');
		});

	});

	describe('parseLine', () => {
		const csvLine = 'Consectetur adipiscing,28.72,10';
		test('should return object with keys from column keys and values from CSV', () => {
			expect(parseLine(csvLine)).toEqual(expect.objectContaining({
				id: expect.any(String),
				name: "Consectetur adipiscing",
				price: 28.72,
				quantity: 10
			}));
		});
	});

	describe('parse', () => {
		const incorrect = `${URL}samples/\incorrect.csv`;
		const consoleError = [
			{
				type: 'cell',
				row: 1,
				column: 1,
				message: 'Expected cell to be a positive number but received "-9.00".'
			}
		];
		test('should return error if file has incorrect data', () => {
			expect(() => {
				parse(incorrect);
			}).toThrow();

			// что в такой реализации неверно и почему тест проваливается?
			// source https://jestjs.io/ru/docs/next/expect#tothrowerror
			// function catchError() {
			// 	parse(incorrect);
			// };
			// expect(catchError).toThrowError(consoleError);
			// expect(catchError).toThrowError(new Error('Validation failed!'));
		});
	});


	describe('calcTotal', () => {
		const cartItems = [
			{
				"id": "3e6def17-5e87-4f27-b6b8-ae78948523a9",
				"name": "Mollis consequat",
				"price": 9,
				"quantity": 2
			},
			{
				"id": "90cd22aa-8bcf-4510-a18d-ec14656d1f6a",
				"name": "Tvoluptatem",
				"price": 10.32,
				"quantity": 1
			}
		];

		test('should return total price of items', () => {
			expect(calcTotal(cartItems)).toBeCloseTo(28.32);
		});
	});

	describe('createError', () => {
		test('should create an object with parameters as keys and arguments as values', () => {
			const type = 'header';
			const row = 2;
			const column = 8;
			const message = 'Expected header';

			const result = {
				type: 'header',
				row: 2,
				column: 8,
				message: 'Expected header',
			}

			expect(createError(type, row, column, message)).toEqual(result);
		})
	})
});

describe('CartParser - integration test', () => {
	// Add your integration test here.
	describe('parse', () => {
		const result = {
			items: [
				{
					id: expect.any(String),
					name: "Mollis consequat",
					price: 9,
					quantity: 2
				},
				{
					id: expect.any(String),
					name: "Tvoluptatem",
					price: 10.32,
					quantity: 1
				},
				{
					id: expect.any(String),
					name: "Scelerisque lacinia",
					price: 18.9,
					quantity: 1
				},
				{
					id: expect.any(String),
					name: "Consectetur adipiscing",
					price: 28.72,
					quantity: 10
				},
				{
					id: expect.any(String),
					name: "Condimentum aliquet",
					price: 13.9,
					quantity: 1
				}
			],
			total: 348.32
		};
		test('should return object with cart items and total price', () => {
			expect(parse(pathToFile)).toEqual(result);
		});
	});
});